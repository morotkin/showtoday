$(document).ready(function () {
	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 100;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 2000);
		e.preventDefault();
	});
	
	$('#send-message').submit(function(e) {
		e.preventDefault();
		var $this = $(this);
		var data = $this.serialize();
		$this.find('input').val('');
		$('#myModal').modal('hide');
		$.ajax({
			type: 'POST',
			url: '/script/send.php',
			data: data,            
			success: function() {
				alert('Ваше сообщение отправлено, в ближайщее время мы с Вами свяжемся.');                
			},
			error: function() {
				alert('Ошибка: Что-то слулось, Вы можете еще раз попробовать?');
			}
		});
	});

});